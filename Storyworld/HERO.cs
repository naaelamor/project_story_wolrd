﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Storyworld
{
    public class HUMAN
    {
        public bool isEnemy;
        public string name;
        public int HP;
        public int MP;
        public int ATK;
        public int MATK;
        public int DEF;
        public Form main;

        public string Picturepart;
        public System.Windows.Forms.PictureBox satan;
        System.Drawing.Point point;

       

        public HUMAN()
        {
            
        }

        public HUMAN(Form main, string name, string Picturepart, System.Drawing.Point point, bool isEnemy)
            : this()
        {
            this.isEnemy = isEnemy;
            this.name = name;
            this.point = point;
            this.Picturepart = Picturepart;

            this.satan = new System.Windows.Forms.PictureBox();
            this.satan.ErrorImage = null;
            this.satan.Image = System.Drawing.Image.FromFile(Picturepart);
            this.satan.Location = point;
            this.satan.Name = name;
            this.satan.Size = new System.Drawing.Size(116, 127);
            this.satan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //this.satan.TabIndex = 1;
            this.satan.TabStop = false;
            this.satan.Click += new System.EventHandler(this.satan_Click);
        }

        public void Hit(int atk)
        {
            
            if ((this.DEF - atk) > this.HP)
            {
                this.HP = 0;


                    MessageBox.Show(String.Format("{0} ถูกโจมตี !!!! เหลือ HP: {1} ตาย!!", this.name, this.HP));
                    Program.form1.gameStatus = "Waiting";
                    Program.form1.sendGameCommand(String.Format("{0} ฝ่ายตรงข้ามตายแล้ว !", this.name));
            }
            else
            {
                //ถ้า ATK น้อยกว่า DEF   ATK จะไม่ทำให้ HP ลด แต่ถ้ามากกว่า จะนำ ATK-DEF แล้วนำไปหักกับ HP
                this.HP -= (atk - this.DEF) < 0 ? 0 : (atk - this.DEF);

                    MessageBox.Show(String.Format("{0} ถูกโจมตี !!!! เหลือ HP: {1} ", this.name, this.HP));

                
               
            }

            //Program.form1.gameStatus = "Ready";

        }

        private void satan_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(name);

            if (!this.isEnemy)
            {
                Program.form1.MaxHPlabel.Text = Convert.ToString(HP);
                Program.form1.MaxMPlabel.Text = Convert.ToString(MP);
                Program.form1.ATKlacbel.Text = Convert.ToString(ATK);
                Program.form1.DEFlabel.Text = Convert.ToString(DEF);

                //Program.form1.gameStatus = "party";
                Program.form1.atk = this.ATK;
            }
            else
            {
                //Program.form1.gameStatus = "enemy";
                Program.form1.enamyName = this.name;

                string comd = "atk#" + Program.form1.atk + "#" + Program.form1.enamyName;

                //MessageBox.Show(Program.form1.gameStatus);
                if (Program.form1.gameStatus == "Ready")
                {
                    //MessageBox.Show("HELLO");
                    Program.form1.sendGameCommand(comd);
                }
                
            }
        }
    }
    public class swordman : HUMAN
    {

        public swordman(Form main, string name, string Picturepart, System.Drawing.Point point, bool isEnemy)
            : base(main,name, Picturepart, point,isEnemy)
        {
            HP = 100;
            MP = 3;
               ATK = 10;
               MATK = 3;
               DEF = 5;
        }
    }
    public class wizard : HUMAN
    {
        public wizard(Form main, string name, string Picturepart, System.Drawing.Point point, bool isEnemy)
            : base(main, name, Picturepart, point,isEnemy)
        {
            HP = 60;
            MP = 100;
            ATK = 3;
            MATK = 10;
            DEF = 5;
        }
     
    }
    public class priest : HUMAN
    {
        public priest(Form main, string name, string Picturepart, System.Drawing.Point point, bool isEnemy)
            : base(main, name, Picturepart, point,isEnemy)
        {
            HP = 70;
            MP = 70;
            ATK = 6;
            MATK = 6;
            DEF = 6;
        }
       
    }
}
