﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storyworld
{
        public class Monster
        {
            public string name = "satan";
            public int HP = 500;
            public int ATK = 10;
            public int DEF = 10;
            public string Picturepart;
            public System.Windows.Forms.PictureBox satan;
            System.Drawing.Point point;


            public Monster(string name, string Picturepart, System.Drawing.Point point)
            {
                this.name = name;
                this.point = point;
                this.Picturepart = Picturepart;

                this.satan = new System.Windows.Forms.PictureBox();
                this.satan.ErrorImage = null;
                this.satan.Image = System.Drawing.Image.FromFile(Picturepart);
                this.satan.Location = point;
                this.satan.Name = name;
                this.satan.Size = new System.Drawing.Size(116, 127);
                this.satan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                //this.satan.TabIndex = 1;
                this.satan.TabStop = false;
                this.satan.Click += new System.EventHandler(this.satan_Click);
            }
          
            private void satan_Click(object sender, EventArgs e)
            {
                MessageBox.Show(this.name);
                
               

            }
        }
}
