﻿namespace Storyworld
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonItem = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.buttonDeferd = new System.Windows.Forms.Button();
            this.buttonAttack = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Status_server = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TP_Server = new System.Windows.Forms.TextBox();
            this.button_Connect = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Server_Port = new System.Windows.Forms.TextBox();
            this.Server_Ip = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Status = new System.Windows.Forms.GroupBox();
            this.DEFlabel = new System.Windows.Forms.Label();
            this.ATKlacbel = new System.Windows.Forms.Label();
            this.MaxMPlabel = new System.Windows.Forms.Label();
            this.MaxHPlabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.HP = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.btnMsg = new System.Windows.Forms.Button();
            this.txtSendMsg = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelStatus.SuspendLayout();
            this.Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(761, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(123, 109);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.buttonItem);
            this.panel2.Controls.Add(this.buttonRun);
            this.panel2.Controls.Add(this.buttonDeferd);
            this.panel2.Controls.Add(this.buttonAttack);
            this.panel2.Location = new System.Drawing.Point(761, 118);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(100, 140);
            this.panel2.TabIndex = 0;
            // 
            // buttonItem
            // 
            this.buttonItem.Location = new System.Drawing.Point(11, 62);
            this.buttonItem.Name = "buttonItem";
            this.buttonItem.Size = new System.Drawing.Size(75, 23);
            this.buttonItem.TabIndex = 3;
            this.buttonItem.Text = "Item";
            this.buttonItem.UseVisualStyleBackColor = true;
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(11, 93);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 23);
            this.buttonRun.TabIndex = 2;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            // 
            // buttonDeferd
            // 
            this.buttonDeferd.Location = new System.Drawing.Point(11, 32);
            this.buttonDeferd.Name = "buttonDeferd";
            this.buttonDeferd.Size = new System.Drawing.Size(75, 23);
            this.buttonDeferd.TabIndex = 1;
            this.buttonDeferd.Text = "Defernd";
            this.buttonDeferd.UseVisualStyleBackColor = true;
            // 
            // buttonAttack
            // 
            this.buttonAttack.Location = new System.Drawing.Point(11, 3);
            this.buttonAttack.Name = "buttonAttack";
            this.buttonAttack.Size = new System.Drawing.Size(75, 23);
            this.buttonAttack.TabIndex = 0;
            this.buttonAttack.Text = "Attack";
            this.buttonAttack.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Status_server);
            this.groupBox1.Location = new System.Drawing.Point(772, 320);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 129);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // Status_server
            // 
            this.Status_server.Location = new System.Drawing.Point(6, 19);
            this.Status_server.Multiline = true;
            this.Status_server.Name = "Status_server";
            this.Status_server.Size = new System.Drawing.Size(188, 104);
            this.Status_server.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TP_Server);
            this.groupBox2.Controls.Add(this.button_Connect);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.Server_Port);
            this.groupBox2.Controls.Add(this.Server_Ip);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(744, 135);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(228, 179);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Control";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "IP Server:";
            // 
            // TP_Server
            // 
            this.TP_Server.Location = new System.Drawing.Point(109, 113);
            this.TP_Server.Name = "TP_Server";
            this.TP_Server.Size = new System.Drawing.Size(110, 20);
            this.TP_Server.TabIndex = 6;
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(40, 71);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(179, 23);
            this.button_Connect.TabIndex = 5;
            this.button_Connect.Text = "Connect";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(40, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Create";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Server_Port
            // 
            this.Server_Port.Location = new System.Drawing.Point(72, 45);
            this.Server_Port.Name = "Server_Port";
            this.Server_Port.Size = new System.Drawing.Size(147, 20);
            this.Server_Port.TabIndex = 3;
            this.Server_Port.Text = "9999";
            // 
            // Server_Ip
            // 
            this.Server_Ip.Location = new System.Drawing.Point(72, 17);
            this.Server_Ip.Name = "Server_Ip";
            this.Server_Ip.Size = new System.Drawing.Size(147, 20);
            this.Server_Ip.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server IP:";
            // 
            // panelStatus
            // 
            this.panelStatus.Controls.Add(this.button4);
            this.panelStatus.Controls.Add(this.button3);
            this.panelStatus.Controls.Add(this.Status);
            this.panelStatus.Location = new System.Drawing.Point(402, 255);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(270, 168);
            this.panelStatus.TabIndex = 2;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(3, 42);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Skill";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Attack";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Status
            // 
            this.Status.Controls.Add(this.DEFlabel);
            this.Status.Controls.Add(this.ATKlacbel);
            this.Status.Controls.Add(this.MaxMPlabel);
            this.Status.Controls.Add(this.MaxHPlabel);
            this.Status.Controls.Add(this.label6);
            this.Status.Controls.Add(this.label5);
            this.Status.Controls.Add(this.label4);
            this.Status.Controls.Add(this.HP);
            this.Status.Location = new System.Drawing.Point(84, 13);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(173, 143);
            this.Status.TabIndex = 0;
            this.Status.TabStop = false;
            this.Status.Text = "Status";
            // 
            // DEFlabel
            // 
            this.DEFlabel.AutoSize = true;
            this.DEFlabel.Location = new System.Drawing.Point(61, 116);
            this.DEFlabel.Name = "DEFlabel";
            this.DEFlabel.Size = new System.Drawing.Size(35, 13);
            this.DEFlabel.TabIndex = 7;
            this.DEFlabel.Text = "label8";
            // 
            // ATKlacbel
            // 
            this.ATKlacbel.AutoSize = true;
            this.ATKlacbel.Location = new System.Drawing.Point(61, 94);
            this.ATKlacbel.Name = "ATKlacbel";
            this.ATKlacbel.Size = new System.Drawing.Size(35, 13);
            this.ATKlacbel.TabIndex = 6;
            this.ATKlacbel.Text = "label7";
            // 
            // MaxMPlabel
            // 
            this.MaxMPlabel.AutoSize = true;
            this.MaxMPlabel.Location = new System.Drawing.Point(61, 71);
            this.MaxMPlabel.Name = "MaxMPlabel";
            this.MaxMPlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxMPlabel.TabIndex = 5;
            this.MaxMPlabel.Text = "label8";
            // 
            // MaxHPlabel
            // 
            this.MaxHPlabel.AutoSize = true;
            this.MaxHPlabel.Location = new System.Drawing.Point(61, 49);
            this.MaxHPlabel.Name = "MaxHPlabel";
            this.MaxHPlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxHPlabel.TabIndex = 4;
            this.MaxHPlabel.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Defent";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Attack";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "MP";
            // 
            // HP
            // 
            this.HP.AutoSize = true;
            this.HP.Location = new System.Drawing.Point(9, 49);
            this.HP.Name = "HP";
            this.HP.Size = new System.Drawing.Size(22, 13);
            this.HP.TabIndex = 0;
            this.HP.Text = "HP";
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(744, 27);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(228, 73);
            this.txtMsg.TabIndex = 4;
            // 
            // btnMsg
            // 
            this.btnMsg.Location = new System.Drawing.Point(931, 106);
            this.btnMsg.Name = "btnMsg";
            this.btnMsg.Size = new System.Drawing.Size(41, 23);
            this.btnMsg.TabIndex = 3;
            this.btnMsg.Text = "send";
            this.btnMsg.UseVisualStyleBackColor = true;
            this.btnMsg.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtSendMsg
            // 
            this.txtSendMsg.Location = new System.Drawing.Point(744, 109);
            this.txtSendMsg.Name = "txtSendMsg";
            this.txtSendMsg.Size = new System.Drawing.Size(181, 20);
            this.txtSendMsg.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(741, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Name:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(784, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(46, 20);
            this.txtName.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtSendMsg);
            this.Controls.Add(this.txtMsg);
            this.Controls.Add(this.btnMsg);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelStatus.ResumeLayout(false);
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button buttonRun;
        public System.Windows.Forms.Button buttonDeferd;
        public System.Windows.Forms.Button buttonAttack;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.Button buttonItem;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox Status_server;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox TP_Server;
        public System.Windows.Forms.Button button_Connect;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox Server_Port;
        public System.Windows.Forms.TextBox Server_Ip;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel panelStatus;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.GroupBox Status;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label HP;
        public System.Windows.Forms.Label DEFlabel;
        public System.Windows.Forms.Label ATKlacbel;
        public System.Windows.Forms.Label MaxMPlabel;
        public System.Windows.Forms.Label MaxHPlabel;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Button btnMsg;
        private System.Windows.Forms.TextBox txtSendMsg;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtName;

    }
}

