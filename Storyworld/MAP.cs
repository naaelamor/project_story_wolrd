﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Storyworld
{
    class MAP
    {
        public List<HUMAN> listHuman;
        private System.Windows.Forms.Panel panel1;
        public MAP()
        {
            this.listHuman = new List<HUMAN>();
            this.panel1 = new System.Windows.Forms.Panel();
            //this.panel1.Visible = false;
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            //this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000,500);
            this.panel1.TabIndex = 0;
            //this.panel1.ResumeLayout(false);
            this.panel1.SendToBack();
            Program.form1.Controls.Add(this.panel1);
        }

        public void addMonster(Monster monster)
        {
            this.panel1.Controls.Add(monster.satan);
        }
        public void addCaracter(HUMAN caracter)
        {
            this.panel1.Controls.Add(caracter.satan);
            this.listHuman.Add(caracter);
        }
    }
}
