﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storyworld
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SocketClient c; SocketServer s;
        bool isServer = true;

        public string gameStatus = "Ready";
        public string enamyName;
        public int atk;
        MAP m;
        

        private void Form1_Load(object sender, EventArgs e)
        {
           c = new SocketClient();
           s = new SocketServer();                      
            
            s.onMessageReceipt += s_onMessageReceipt;
            c.onMessageReceipt += c_onMessageReceipt;

            TP_Server.Text = s.ServerIP;
            //MessageBox.Show(Environment.CurrentDirectory);
            m = new MAP();
            //m.addMonster(new Monster("Duck", @"D:\clumsy-smurf-icon.png", new Point(200, 200)));
            //m.addMonster(new Monster("Dog", @"D:\clumsy-smurf-icon.png", new Point(400, 200)));
            m.addCaracter(new wizard(this, "Kali", Environment.CurrentDirectory + @"\Charecter\Magician.png", new Point(40, 270),false));
            m.addCaracter(new swordman(this, "Archer", Environment.CurrentDirectory + @"\Charecter\Priest.png", new Point(150, 270), false));
            m.addCaracter(new priest(this, "Tinkerer", Environment.CurrentDirectory + @"\Charecter\Swordman.png", new Point(260, 270), false));
            m.addCaracter(new swordman(this, "0", Environment.CurrentDirectory + @"\Charecter\Magician.png", new Point(430, 50), true));
            m.addCaracter(new priest(this, "1", Environment.CurrentDirectory + @"\Charecter\Priest.png", new Point(290, 50),true));
            m.addCaracter(new wizard(this, "2", Environment.CurrentDirectory + @"\Charecter\Swordman.png", new Point(530, 50), true));
           
        }


        private void c_onMessageReceipt(string msg)
        {
            string prefix = msg.Substring(0, 3);
            if (prefix == "msg")
            {
                string message = msg.Substring(3, msg.Length - 4);
                AppendtxtMsg(message);
            }
            else if (prefix == "atk")
            {
                string[] data = msg.Split('#');

                int index = int.Parse(data[2]);
                Program.form1.gameStatus = "Ready";
                //MessageBox.Show(index.ToString());
                m.listHuman[index].Hit(int.Parse(data[1]));
            }
            else if (prefix == "sta")
            {
                string message = msg.Substring(3, msg.Length - 4);
                MessageBox.Show(message);
            }
        }

        private void s_onMessageReceipt(string msg)
        {
            string prefix = msg.Substring(0, 3);
            if (prefix == "msg")
            {
                string message = msg.Substring(3, msg.Length - 4);
                AppendtxtMsg(message);
            }
            else if (prefix == "atk")
            {
                    string[] data = msg.Split('#');

                    int index = int.Parse(data[2]);

                    //MessageBox.Show(index.ToString());
                    m.listHuman[index].Hit(int.Parse(data[1]));

            }  
        }

        public delegate void UpdatetxtMsgCallback(string text);

        private void AppendtxtMsg(string msg) 
        {
        if (InvokeRequired) 
        {
        // We cannot update the GUI on this thread.
        // All GUI controls are to be updated by the main (GUI) thread.
        // Hence we will use the invoke method on the control which will
        // be called when the Main thread is free
        // Do UI update on UI thread
        object[] pList = {msg};
        txtMsg.BeginInvoke(new UpdatetxtMsgCallback(OnUpdateRichEdit), pList);
        }
        else
         {
        // This is the main thread which created this control, hence update it
        // directly 
        OnUpdateRichEdit(msg);
        }
        }

        private void OnUpdateRichEdit(string msg)
        {
            txtMsg.AppendText(msg);
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            txtName.Text = "Client";
            isServer = false;
            c.ConnectNetwork(Server_Ip.Text, Server_Port.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtName.Text = "Server";
            s.StartListen(Server_Port.Text); 
            Status_server.Text ="Create Server IP : " + s.ServerIP + Environment.NewLine + "Port : " + Server_Port.Text ;  
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (isServer)
            {
                s.SendMsgToClient("msg" + txtName.Text +  ": " +  txtSendMsg.Text, 1);
            }
            else
            {
                c.SendMessage("msg" + txtName.Text + ": " + txtSendMsg.Text);
            }
        }

        public void sendGameCommand(string msg)
        {
            if (isServer)
            {
                s.SendMsgToClient(msg, 1);
            }
            else
            {
                c.SendMessage(msg);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            
            
            
            
        }
    }
}
